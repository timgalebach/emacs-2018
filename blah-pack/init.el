;; User pack init file
;;
;; Use this file to initiate the pack configuration.
;; See README for more information.

;; Load bindings config
(live-load-config-file "bindings.el")

;;Added by Tim
(add-to-list 'package-archives
'("melpa-stable" . "http://melpa-stable.milkbox.net/packages/") t)

;;Golang
(add-hook 'before-save-hook 'gofmt-before-save)
(setq compile-command "go build -v")
